#Demo Introduction

A websocket server generates a 6 digit random number every 10 seconds and pushes it to the connected clients.
Browser clients register with the websocket server with a clientid. 
Connected Clients receive the random number every 10 seconds from the server and displays it in a text box.
The example demonstrates server push. Browser Clients do not push anything to the server.

Required node modules are bundled with the example. Running this example requires node installation.

##Software Setup
Windows 7 \n
node v4.4.5 \n  
Chrome v62 


##Running the example
###1.Copy the folder node_ws_push to a <path>

###2.Start websocket server - execute following commands
cd <path>/node_ws_push/node_ws_server \n
node wsMain.js \n

###3.Start websocket client
Open <path>/node_ws_push/browser_ws_client/random.html in chrome browser_ws_client \n
Open chrome developer console (press F12) \n
Click on "connect" button \n

You should see new random numbers every 10 seconds in the textbox of the html page.

Feel free to change and inspect the code!