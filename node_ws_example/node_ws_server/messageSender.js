//import websocketServer.js
// this import script starts the websocketServer
var websocketServer = require("./websocketServer");

//get the connected clients list, initially this list is empty
var clients = websocketServer.clients;

//log the server id - not much technical significance
console.log("Server ID :: "+websocketServer.serverId);

//invoke sendRandomNumber function every 10000 ms
setInterval(sendRandomNumber, 10000);

// generate and send a random number to all connected clients
function sendRandomNumber() {

    //when no clients are connected, will simply log and return
    if(clients==null || clients.length==0) {
        console.log("No Connected Clients");
        return;
    }
    //else, we generate a 6 digit random number and broadcast to all the connected clients
    randomNumber = Math.floor((Math.random() * 100000) + 1);
    clients.forEach(function(client){
        client.connection.sendUTF(
        JSON.stringify({random: randomNumber}));
    console.log("Message Sent To Client :: " + client.clientid);
    });
    
}