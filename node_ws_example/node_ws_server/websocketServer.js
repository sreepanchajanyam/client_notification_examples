var WebSocketServer = require('websocket').server;
var http = require('http');
var url = require("url");

var server = http.createServer(function(request, response) {
  // process HTTP request. Since we're writing just WebSockets
  // server we don't have to implement anything.
});
server.listen(8000, function() { });
console.log("Started websocket server on port :: "+8000);
// create the server
wsServer = new WebSocketServer({
  httpServer: server
});
 var clients = [ ];
// On WebSocket connection request from client, do the following
wsServer.on('request', function(request) {
  //accept the connection
  var connection = request.accept(null, request.origin);
  
  //parse the connection URL used by the client
  var parsedUrl = url.parse(request.resource, true); // true to get query as object
  
  //extract the query parameters as an object from the parsed URL
  var queryAsObject = parsedUrl.query;

  //get the clientid from query parameters, if empty initialize clientid to a default value
var clientid = queryAsObject.clientid || "dfalt-"+(Date.now()).toString().substr(8,5);
  console.log("Client Connected - ClientID :: " + clientid);
  
  //save the connection reference and clientid to list of connected clients.
  //save the index of the connection object
  var index = clients.push({connection:connection,clientid:clientid}) - 1;


  //when client sends a message to us (the websocket server) -- our example is a server side push, so we are not using this
  connection.on('message', function(message) {
    if (message.type === 'utf8') {
      console.log("User Sent a message" + message.data);
    }
  });

 // when client closes the connection
  connection.on('close', function(connection) {
    console.log("connection closed");
    //remove the clientobject from the specified index, so that message sender will not send messages to disconnected clients.
    clients.splice(index,1);
  });
});
module.exports = {"clients":clients,"serverId":"chatserver-1"};