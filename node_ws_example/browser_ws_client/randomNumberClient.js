var connection;
function connect() {
    window.WebSocket = window.WebSocket || window.MozWebSocket;
    var randomNumberTextBox = $("#randomNumberTextBox");
    if (!window.WebSocket) {
        alert("Your Browser Does Not support WebSocket");
        return;
    }

    //generate a random clientid
    var clientid = makeid();
    
    //establish connection to localhost at 8000 port with the generated clientid
    connection = new WebSocket("ws://127.0.0.1:8000?clientid=" + clientid);
    
    //callback when connection is established
    connection.onopen = function () {
        //you should see the below log statement in browser console when you click on "connect" button on the html page
        console.log("Connection opened, clientid :: "+clientid);
    };
    	
    //callback when server pushes a message to us(websocket client)
    connection.onmessage = function (messageEvent) {
        //parse the data
        jsonData = JSON.parse(messageEvent.data);
        //log the data
        console.log("new message received ::" + JSON.stringify(messageEvent.data));
        //show the 6 digit random number in the text box
        randomNumberTextBox.val(jsonData.random);
    };

    connection.onclose = function (closeEvent) {
        console.log("connection closed, code :: " + closeEvent.code  + ", reason::"+closeEvent.reason + ",  clean close::" +closeEvent.wasClean);
    };

}

function disconnect() {
    console.log("disconnect called");
    connection.close()
}


function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    text += "-" + Math.floor(Date.now()).toString().substr(8, 5);
    return text;
}