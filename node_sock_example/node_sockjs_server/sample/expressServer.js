var sockjs = require('sockjs');
var path = require('path');
var express = require('express');
var server = express();
var serveStatic = require('serve-static');

//  setup a webserver to serve static content on 9090 port
server.use('/client',express.static(path.join(__dirname, 'client')));
server.listen(9090);
console.log("Listening 9090");