var sockjs = require('sockjs');
var http = require('http');
var url = require("url");
var util = require("util");
var path = require('path');
var express = require('express');
var app = express();

// serve client assets from path "/client"
app.use('/client',express.static(path.join(__dirname, 'client')));
var sockServer = sockjs.createServer();

var clients = [];
// On WebSocket connection request from client, do the following
sockServer.on('connection', function (connection) {

    // log the complete object structure of connection object, verbose - turn on when needed
    // console.log("connection"+util.inspect(connection));

    var index;

    //when client sends a message to us (the websocket server) -- our example is a server side push, so we are not using this
    connection.on('data', function (message) {
        console.log("User Sent a message" + message);
        var messageObj = JSON.parse(message);
        if (messageObj.type == 'handshake') {
            clientid = messageObj.clientid;
            console.log("Client Connected - ClientID :: " + clientid);
            index = clients.push({ connection: connection, clientid: clientid }) - 1;
        } else {
            console.log("User Sent a message");
        }
    });

    // when client closes the connection
    connection.on('close', function () {
        console.log("connection closed");
        //remove the clientobject from the specified index, so that message sender will not send messages to disconnected clients.
        clients.splice(index, 1);
    });
});

// listen on 8001 and get the instance of http app server
var server = app.listen(8001);
// register sockjs on /randomNumber
sockServer.installHandlers(server, { prefix: '/randomNumber' })
console.log("Started sockjs server on port :: " + 8001);
module.exports = { "clients": clients, "serverId": "chatserver-1" };