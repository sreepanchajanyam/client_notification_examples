var sock;
function connect() {

    var randomNumberTextBox = $("#randomNumberTextBox");

    //generate a random clientid
    var clientid = makeid();

    // xdr-streaming and xdr-polling do not work
    var options = { transports: ['websocket', 'iframe-eventsource', 'xhr-streaming', 'xhr-polling', 'xdr-streaming', 'xdr-polling'] };

    /**
     * try all the below options
     */

    // websocket default option
    var option1 = { transports: ['websocket'] };
    
    // eventsource - these are also called server-sent events,only 1 get request to the server
    // server keeps the connection open and pushes the messages on it
    var option2 = { transports: ['iframe-eventsource'] };
    
    // Streaming - one get request is sent to the server and server keeps the connection open
    var option3 = { transports: ['xhr-streaming'] };
    
    // Long Polling - a request is sent to the server and server keeps the request open for an update
    var option4 = { transports: ['xhr-polling'] };


    // replace the options parameter with option1 to option4 are check request/response in browser console
    sock = new SockJS('http://localhost:8001/randomNumber', null, option1);

    //callback when connection is established
    sock.onopen = function () {
        //you should see the below log statement in browser console when you click on "connect" button on the html page
        console.log("Connection opened, clientid :: " + clientid);
        sock.send(JSON.stringify({ type: "handshake", clientid: clientid }));
    };

    //callback when server pushes a message to us(sockjs browser client)
    sock.onmessage = function (messageEvent) {
        //parse the data
        jsonData = JSON.parse(messageEvent.data);
        //log the data
        console.log("new message received ::" + JSON.stringify(messageEvent.data));
        //show the 6 digit random number in the text box
        randomNumberTextBox.val(jsonData.random);
    };

    sock.onclose = function (closeEvent) {
        console.log("connection closed, code :: " + closeEvent.code + ", reason::" + closeEvent.reason + ",  clean close::" + closeEvent.wasClean);
    };

}

function disconnect() {
    console.log("disconnect called");
    sock.close()
}

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    text += "-" + Math.floor(Date.now()).toString().substr(8, 5);
    return text;
}