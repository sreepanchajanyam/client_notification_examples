var http = require('http')
var url = require('url')
var fs = require('fs')
var path = require('path');

http.createServer(function (request, response) {
    var requestUrl = url.parse(request.url);
    console.log(requestUrl);
    response.writeHead(200);
    fs.createReadStream(path.join(__dirname,requestUrl.pathname)).pipe(response)  // do NOT use fs's sync methods ANYWHERE on production (e.g readFileSync) 
}).listen(8080)    